# Image Embedder

Userscript for embedding encrypted data in image hosts.

## Supported sites

- [Imgur](https://imgur.com/)
- [ImgBB](https://imgbb.com/)
- [Postimages](https://postimages.org/)

## Uploading

In order to upload data you must be on the upload page of the respective site.

Select **Upload** from the userscript menu. (refresh the page if the Upload entry doesn't appear on Imgur)

Select PNGs for mask files. These are images that will be visible. The smaller, the better, as that leaves more space for the payload.

The payload is self explanatory.

You can select a larger AES key size if you wish.

When the script finishes encrypting, a "share code" will be displayed in a textbox. Click it to select it, and copy it. This is the code for decrypting and downloading the file.

When you copy the share code, the images will be posted.

## Downloading

In order to download, you select **Download** from the userscript menu while on the post's page. (For ImgBB, since it only supports single image posts, you do this on the homepage and you will be prompted to input the links)

Input the share code and click download. The images will be fetched and the file will be decrypted and downloaded.
