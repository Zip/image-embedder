// ==UserScript==
// @name        Image Embedder
// @match       https://imgur.com/*
// @match       https://imgbb.com/*
// @match       https://postimages.org/*
// @match       https://postimg.cc/gallery/*
// @match       https://pixxxels.cc/gallery/*
// @grant       GM_registerMenuCommand
// @grant       GM.registerMenuCommand
// @grant       GM_xmlhttpRequest
// @grant       GM.xmlHttpRequest
// @version     0.1.2
// @author      Zip
// @updateURL   https://codeberg.org/Zip/image-embedder/raw/branch/master/image-embedder.user.js
// @downloadURL https://codeberg.org/Zip/image-embedder/raw/branch/master/image-embedder.user.js
// @description Encrypts, embeds into PNGs, and uploads files to image hosts
// @license     GPL-3.0
// ==/UserScript==

const registerMenuCommand = typeof GM_registerMenuCommand != 'undefined' ? GM_registerMenuCommand : (GM ? GM.registerMenuCommand : GM_registerMenuCommand);
const httpRequest = typeof GM_xmlhttpRequest != 'undefined' ? GM_xmlhttpRequest : (GM ? GM.xmlHttpRequest : GM_xmlhttpRequest);

const CRC32 = (function(){
    var table = new Uint32Array(256);
    for(var i=256; i--;){
        var tmp = i;

        for(var k=8; k--;){
            tmp = tmp & 1 ? 3988292384 ^ tmp >>> 1 : tmp >>> 1;
        }

        table[i] = tmp;
    }

    /**
     * Calculates CRC-32
     * @param {Uint8Array}   data Text byte length limit
     * @returns {Uint8Array} Return of the CRC
     */
    return function(data){
        var crc = -1;

        for(var i=0, l = data.length; i < l; i++){
            crc = crc >>> 8 ^ table[ crc & 255 ^ data[i] ];
        }

        crc = (crc ^ -1) >>> 0;
        return Uint8Array.from([crc >> 24 & 0xFF, crc >> 16 & 0xFF, crc >> 8 & 0xFF, crc & 0xFF]);
    };
})();

/**
 * Compares two Uint8Arrays
 * @param {Uint8Array} array1
 * @param {Uint8Array} array2
 * @returns {boolean}  Whether the arrays are the same
 */
function compareUint8Array(array1, array2){
    if(array1.length != array2.length) return false;
    for(let i = 0; i < array1.length; i++){
        if(array1[i] != array2[i]) return false;
    }
    return true;
}

class PNGChunk{
    /**
     * Parse a PNG Chunk
     * @param {Uint8Array} array The chunk content (size, type, data and CRC);
     */
    constructor(array){
        this.array = array;

        this.length = PNGChunk.testLength(array, 0);

        const decoder = new TextDecoder();
        this.typeArrray = array.subarray(4, 8);
        this.type = decoder.decode(this.typeArrray);

        this.data = array.subarray(8, 8 + this.length);

        this.crc = array.subarray(8 + this.length, 8 + this.length + 4);

        this.crcCorrect = compareUint8Array(CRC32(array.subarray(4, 8 + this.length)), this.crc);
    }

    /**
     * Tests the length of a PNG chunk
     * @param {Uint8Array} array The array in which the chunk exists
     * @param {number}     index The index in the array at which the chunk starts
     * @returns {number}   Length of the chunk data
     */
    static testLength(array, index){
        const lengthArray = array.subarray(index, index + 4);
        return (lengthArray[0] << 24) | (lengthArray[1] << 16) | (lengthArray[2] << 8) | lengthArray[3];
    }

    /**
     * Creates a new PNG chunk, a copy of the buffer will be created
     * @param {string}     type The type of the chunk
     * @param {Uint8Array} data The data to put in the chunk
     * @returns {PNGChunk} The created PNG chunk
     */
    static create(type, data){
        const encoder = new TextEncoder()
        const typeArray = encoder.encode(type);
        if(typeArray.length != 4) return null;

        const chunkArray = new Uint8Array(data.length + 12);

        chunkArray[0] = data.length >> 24 & 0xFF;
        chunkArray[1] = data.length >> 16 & 0xFF;
        chunkArray[2] = data.length >> 8 & 0xFF;
        chunkArray[3] = data.length & 0xFF;

        chunkArray.set(typeArray, 4);

        chunkArray.set(data, 8);

        const crc = CRC32(chunkArray.subarray(4, 8 + data.length));
        chunkArray.set(crc, 8 + data.length);

        return new PNGChunk(chunkArray);
    }
}

class PNG{
    /**
     * Create a PNG, a copy of the buffer will be created
     * @param {Uint8Array} array Data to load the PNG from
     */
    constructor(array){
        this.header = new Uint8Array([0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A]);
        this.initialized = true;
        for(let i = 0; i < this.header.length; i++){
            if(array[i] != this.header[i]){
                throw new Error("Not a PNG file");
            }
        }
        
        this.chunks = [];
        for(let i = 8; i < array.length;){
            let chunkLength = PNGChunk.testLength(array, i) + 12;
            let chunk = new PNGChunk(array.slice(i, i + chunkLength));
            if(!chunk.crcCorrect){
                throw new Error('CRC incorrect');
            }
            if(chunk.type == 'IEND'){
                this.endChunk = chunk;
            }else{
                this.chunks.push(chunk);
            }
            i += chunkLength;
        }
    }

    /**
     * Exports the PNG file
     * @returns {Uint8Array} The exported PNG file
     */
    export(){
        let totalLength = this.header.length + this.endChunk.array.length;
        for(let chunk of this.chunks) totalLength += chunk.array.length;

        const result = new Uint8Array(totalLength);

        let pos = 0;
        result.set(this.header, 0);
        pos = this.header.length;

        for(let i = 0; i < this.chunks.length; i++){
            result.set(this.chunks[i].array, pos);
            pos += this.chunks[i].array.length;
        }

        result.set(this.endChunk.array, pos);

        return result;
    }
}

function requestText(url){
    return new Promise((resolve, reject)=>{
        httpRequest({
            method: "GET",
            url,
            responseType: 'text',
            onload: function(response) {
                resolve(response.response);
            },
            onerror: reject
          });
    });
}

function requestArrayBuffer(url){
    return new Promise((resolve, reject)=>{
        httpRequest({
            method: "GET",
            url,
            responseType: 'arraybuffer',
            onload: function(response) {
                resolve(response.response);
            },
            onerror: reject
          });
    });
}

function binaryToBase64(data){
    return new Promise(resolve=>{
        const reader = new FileReader();
        reader.onload = function(){
            resolve(this.result.split('base64,').pop());
        }
        reader.readAsDataURL(new Blob([data]))
    });
}

function base64ToBinary(base) {
    const str = atob(base);
    const bufView = new Uint8Array(str.length);
    for (let i = 0, strLen = str.length; i < strLen; i++) {
        bufView[i] = str.charCodeAt(i);
    }
    return bufView;
}

function importBase64Key(key){
    const raw = base64ToBinary(key);
    return window.crypto.subtle.importKey(
        'raw',
        raw,
        'AES-GCM',
        true,
        ['encrypt', 'decrypt']
    );
}

async function exportBase64Key(key){
    const exported = await window.crypto.subtle.exportKey('raw', key);
    return await binaryToBase64(exported);
}

function generateKey(length){
    return window.crypto.subtle.generateKey(
        {
          name: 'AES-GCM',
          length
        },
        true,
        ['encrypt', 'decrypt']
    );
}

async function embedEncryptContent(filename, data, key){
    const encodedFilename = new TextEncoder().encode(filename);
    const filenameHash = await crypto.subtle.digest('SHA-256', encodedFilename);
    return window.crypto.subtle.encrypt(
        {
          name: "AES-GCM",
          iv: filenameHash
        },
        key,
        data
    );
}

async function decryptEmbed(filename, encrypted, key){
    const encodedFilename = new TextEncoder().encode(filename);
    const filenameHash = await crypto.subtle.digest('SHA-256', encodedFilename);
    return window.crypto.subtle.decrypt(
        {
          name: "AES-GCM",
          iv: filenameHash
        },
        key,
        encrypted
    );
}

function readFile(file){
    return new Promise(resolve=>{
        const reader = new FileReader();
        reader.onload = function(){
            resolve(new Uint8Array(this.result));
        }
        reader.readAsArrayBuffer(file);
    });
}

function downloadFile(data, filename){
    const blob = new Blob([data]);
    const downloadBtn = document.createElement('a');
    downloadBtn.href = URL.createObjectURL(blob);
    downloadBtn.download = filename;
    document.body.appendChild(downloadBtn);
    downloadBtn.click();
    downloadBtn.remove();
}

function embedPNG(mask, payload){
    const packed = new PNG(mask);
    const payloadChunk = PNGChunk.create('IDAT', payload);
    packed.chunks.push(payloadChunk);
    return {data: packed.export(), crc: payloadChunk.crc};
}

const maxPNGSize = ({
    'imgur.com': 5000000,
    'imgbb.com': 32000000,
    'postimages.org': 24000000
})[window.location.hostname]; 

async function createSplitEmbeds(maskFiles, payload){
    let payloadIdx = 0;
    let chunkIdx = 0;
    const container = new DataTransfer();
    while(payloadIdx < payload.length){
        let maskFile = maskFiles.shift();
        if(!maskFile) return;
        let spaceLeft = maxPNGSize - maskFile.size - 12 - 2; // 12 bytes for PNG chunk data and 2 bytes for sequence index
        if(spaceLeft <= 0) continue;

        let payloadData = payload.subarray(payloadIdx, payloadIdx + spaceLeft);
        payloadIdx += payloadData.length;

        let payloadChunk = new Uint8Array(payloadData.length + 2);
        payloadChunk.set(payloadData, 2);
        payloadChunk[0] = (chunkIdx >> 8) & 0xFF;
        payloadChunk[1] = chunkIdx & 0xFF;

        let maskData = await readFile(maskFile);
        let embedded = embedPNG(maskData, payloadChunk);
        let file = new File([embedded.data], maskFile.name, {type: 'image/png', lastModified: Date.now()});
        console.log(file.name, file.size);
        container.items.add(file);
        chunkIdx++;
    }
    return container;
}

async function createShareCode(filename, numFiles, key){
    const encodedFilename = new TextEncoder().encode(filename);
    const shareCode = await binaryToBase64(encodedFilename) + '_' + numFiles.toString() + '_' + key;
    return shareCode;
}

async function parseShareCode(code){
    const parts = code.split('_');

    const binaryFilename = base64ToBinary(parts[0]);
    const filename = new TextDecoder().decode(binaryFilename);

    const numFiles = Number(parts[1]);

    const key = await importBase64Key(parts[2]);

    return {filename, numFiles, key};
}

const postImages = ({
    'imgur.com': function(files){
        const fileInput = document.getElementById('file-input');
        fileInput.files = files;
        fileInput.dispatchEvent(new Event('change', {bubbles: true}));
    },
    'imgbb.com': function(files){
        const fileInput = [...document.getElementsByTagName('input')].find(elem=>elem.type=='file' && elem.className.includes('hidden'));
        fileInput.files = files;
        fileInput.dispatchEvent(new Event('change', {bubbles: true}));
    },
    'postimages.org': function(files){
        const fileInput = [...document.getElementsByTagName('input')].find(elem=>elem.type=='file' && elem.className.includes('hidden'));
        fileInput.files = files;
        fileInput.dispatchEvent(new Event('change', {bubbles: true}));
    }
})[window.location.hostname];

const fetchImages = ({
    'imgur.com': async function(statusLabel, numFiles){
        statusLabel.textContent = 'Getting API client ID...';
    
        const clientID = await getImgurClientID();
        const res = await requestText(`https://api.imgur.com/post/v1/albums/${document.location.pathname.split('/').pop()}?client_id=${clientID}&include=media%2Cadconfig%2Caccount`);
        const postInfo = JSON.parse(res);
    
        if(!postInfo){
            statusLabel.remove();
            alert("Failed to get post info");
            return;
        }
    
        const images = postInfo.media;
    
        if(numFiles != images.length){
            statusLabel.remove();
            alert("Number of images doesn't match the expected number");
            return;
        }
    
        statusLabel.textContent = 'Fetching image 1/' + images.length;
    
        const payloadChunks = [];
        let resolveImages;
        const waiter = new Promise(resolve=>{
            resolveImages = resolve;
        });
        images.forEach(async image=>{
            let buffer = await requestArrayBuffer(image.url);
            let array = new Uint8Array(buffer);
            let png = new PNG(array);
            let embedDataChunk = png.chunks.pop();
            payloadChunks.push(embedDataChunk.data);
            statusLabel.textContent = `Fetching image ${payloadChunks.length + 1}/${images.length}`;
            if(payloadChunks.length == images.length) resolveImages();
        });
        await waiter;
    
        if(numFiles != payloadChunks.length){
            statusLabel.remove();
            alert("Failed to fetch all images");
            return;
        }
    
        return payloadChunks;
    },
    'imgbb.com': async function(statusLabel, numFiles){
        const links = prompt('ImgBB links').trim();
        if(!links){
            statusLabel.remove();
            alert("Failed to fetch all images");
            return;
        };
        const splitLinks = links.split(/\s+/);
        if(splitLinks.length != numFiles){
            statusLabel.remove();
            alert("Number of links doesn't match the expected number");
            return;
        }
    
        statusLabel.textContent = 'Fetching image 1/' + splitLinks.length;
    
        const payloadChunks = [];
        let resolveImages;
        const waiter = new Promise(resolve=>{
            resolveImages = resolve;
        });
        splitLinks.forEach(async image=>{
            const text = await requestText(image);
            const page = new DOMParser().parseFromString(text, 'text/html');
            const imgMeta = [...page.getElementsByTagName('meta')].find(elem=>elem.getAttribute('property') == 'og:image');
    
            let buffer = await requestArrayBuffer(imgMeta.content);
            let array = new Uint8Array(buffer);
            let png = new PNG(array);
            let embedDataChunk = png.chunks.pop();
            payloadChunks.push(embedDataChunk.data);
            statusLabel.textContent = `Fetching image ${payloadChunks.length + 1}/${splitLinks.length}`;
            if(payloadChunks.length == numFiles) resolveImages();
        });
        await waiter;
    
        if(numFiles != payloadChunks.length){
            statusLabel.remove();
            alert("Failed to fetch all images");
            return;
        }
    
        return payloadChunks;
    },
    'postimg.cc': async function(statusLabel, numFiles){
        const linkSelector = document.getElementById('embed_box');
        linkSelector.selectedIndex = 1;
        linkSelector.dispatchEvent(new Event('change', {bubbles: true}));
        const linkBox = document.getElementById('code_box');
        const links = linkBox.value.trim();

        const splitLinks = links.split(/\s+/);
        if(splitLinks.length != numFiles){
            statusLabel.remove();
            alert("Number of links doesn't match the expected number");
            return;
        }
    
        statusLabel.textContent = 'Fetching image 1/' + splitLinks.length;
    
        const payloadChunks = [];
        let resolveImages;
        const waiter = new Promise(resolve=>{
            resolveImages = resolve;
        });
        splitLinks.forEach(async image=>{
            let buffer = await requestArrayBuffer(image);
            let array = new Uint8Array(buffer);
            let png = new PNG(array);
            let embedDataChunk = png.chunks.pop();
            payloadChunks.push(embedDataChunk.data);
            statusLabel.textContent = `Fetching image ${payloadChunks.length + 1}/${splitLinks.length}`;
            if(payloadChunks.length == numFiles) resolveImages();
        });
        await waiter;
    
        if(numFiles != payloadChunks.length){
            statusLabel.remove();
            alert("Failed to fetch all images");
            return;
        }
    
        return payloadChunks;
    },
    'pixxxels.cc': async function(statusLabel, numFiles){
        const linkSelector = document.getElementById('embed_box');
        linkSelector.selectedIndex = 1;
        linkSelector.dispatchEvent(new Event('change', {bubbles: true}));
        const linkBox = document.getElementById('code_box');
        const links = linkBox.value.trim();

        const splitLinks = links.split(/\s+/);
        if(splitLinks.length != numFiles){
            statusLabel.remove();
            alert("Number of links doesn't match the expected number");
            return;
        }
    
        statusLabel.textContent = 'Fetching image 1/' + splitLinks.length;
    
        const payloadChunks = [];
        let resolveImages;
        const waiter = new Promise(resolve=>{
            resolveImages = resolve;
        });
        splitLinks.forEach(async image=>{
            let buffer = await requestArrayBuffer(image);
            let array = new Uint8Array(buffer);
            let png = new PNG(array);
            let embedDataChunk = png.chunks.pop();
            payloadChunks.push(embedDataChunk.data);
            statusLabel.textContent = `Fetching image ${payloadChunks.length + 1}/${splitLinks.length}`;
            if(payloadChunks.length == numFiles) resolveImages();
        });
        await waiter;
    
        if(numFiles != payloadChunks.length){
            statusLabel.remove();
            alert("Failed to fetch all images");
            return;
        }
    
        return payloadChunks;
    }
})[window.location.hostname];

function createUploadWindow(){
    const uploadWindow = document.createElement('div');
    uploadWindow.setAttribute('style', 'display: none; position: fixed; top: 0px; left: 0px; background-color: black; color: white; padding: 8px; border: 2px solid white; z-index: 999999;');

    uploadWindow.insertAdjacentHTML('beforeend', '<label>Mask files</label><br>');
    const maskUpload = document.createElement('input');
    maskUpload.type = 'file';
    maskUpload.accept = '.png'
    maskUpload.multiple = 'multiple';
    maskUpload.style.backgroundColor = 'black';
    uploadWindow.appendChild(maskUpload);
    uploadWindow.insertAdjacentHTML('beforeend', '<br><br>');

    uploadWindow.insertAdjacentHTML('beforeend', '<label>Payload</label><br>');
    const payloadUpload = document.createElement('input');
    payloadUpload.type = 'file';
    payloadUpload.style.backgroundColor = 'black';
    uploadWindow.appendChild(payloadUpload);
    uploadWindow.insertAdjacentHTML('beforeend', '<br><br>');

    uploadWindow.insertAdjacentHTML('beforeend', '<label>AES key size</label><br>');
    const keySize = document.createElement('select');
    keySize.appendChild(new Option('128 bit', '128', true));
    keySize.appendChild(new Option('192 bit', '192', false));
    keySize.appendChild(new Option('256 bit', '256', false));
    uploadWindow.appendChild(keySize);
    uploadWindow.insertAdjacentHTML('beforeend', '<br><br>');

    const encryptButton = document.createElement('button');
    encryptButton.textContent = 'Encrypt'
    uploadWindow.appendChild(encryptButton);
    uploadWindow.insertAdjacentHTML('beforeend', '<br><br>');

    const outputBox = document.createElement('textarea');
    outputBox.setAttribute('readonly', '');
    outputBox.style.display = 'none';
    outputBox.onclick = function(){
        this.select();
    }
    uploadWindow.appendChild(outputBox);

    encryptButton.onclick = async function(){
        this.disabled = true;
        const maskFiles = [...maskUpload.files];
        if(maskFiles.length == 0){
            this.disabled = false;
            return;
        }

        const payloadFile = payloadUpload.files[0];
        if(!payloadFile){
            this.disabled = false;
            return;
        }
        const payload = await readFile(payloadFile);

        const key = await generateKey(Number(keySize.value));
        const encryptedBuffer = await embedEncryptContent(payloadFile.name, payload, key);
        const encryptedPayload = new Uint8Array(encryptedBuffer);
        const exportedKey = await exportBase64Key(key);
        
        const splitEmbeds = await createSplitEmbeds(maskFiles, encryptedPayload);
        if(!splitEmbeds){
            alert('Not enough mask files to contain the payload');
            this.disabled = false;
            return;
        }

        outputBox.value = await createShareCode(payloadFile.name, splitEmbeds.files.length, exportedKey);
        outputBox.style.display = '';

        outputBox.addEventListener('copy', ()=>{
            postImages(splitEmbeds.files);
        }, {once: true});

        this.disabled = false;
    }

    document.body.appendChild(uploadWindow);

    return uploadWindow;
}

function createStatusLabel(){
    const label = document.createElement('label');
    label.setAttribute('style', 'position: fixed; top: 0px; left: 0px; background-color: black; color: white; padding: 8px; border: 2px solid white; z-index: 999999;');
    document.body.appendChild(label);
    return label;
}

async function getImgurClientID(){
    const scriptSrc = [...document.getElementsByTagName('script')].find(script=>script.src.includes('/main.')).src;
    const code = await requestText(scriptSrc);
    const matches = code.match(/(?<=,s=")[a-f0-9]+(?=")/g);
    return matches[0];
}

async function downloadFunc(){
    const statusLabel = createStatusLabel();
    statusLabel.textContent = 'Inputting share code...'

    const shareCode = prompt('Share code');
    if(!shareCode){
        statusLabel.remove();
        return;
    }
    const {filename, numFiles, key} = await parseShareCode(shareCode);

    const payloadChunks = await fetchImages(statusLabel, numFiles);

    if(!payloadChunks) return;

    statusLabel.textContent = 'Decrypting data...';

    payloadChunks.sort(function(a, b){
        const idxA = (a[0] << 8) | a[1];
        const idxB = (b[0] << 8) | b[1];
        return idxA - idxB;
    });

    let totalSize = 0;
    for(let chunk of payloadChunks){
        totalSize += chunk.length - 2;
    }

    const encryptedData = new Uint8Array(totalSize);
    let idx = 0;
    for(let payloadChunk of payloadChunks){
        let payloadChunkData = payloadChunk.subarray(2);
        encryptedData.set(payloadChunkData, idx);
        idx += payloadChunkData.length;
    }

    const decrypted = await decryptEmbed(filename, encryptedData, key).catch(err=>{
        statusLabel.remove();
        alert('Failed to decrypt embed\n' + err);
    });

    if(decrypted) downloadFile(decrypted, filename);

    statusLabel.remove();
}

let uploadWindow;

switch(window.location.hostname){
    case 'imgur.com':
        if(document.location.pathname == '/upload'){
            registerMenuCommand('Upload', function(){
                if(!uploadWindow) uploadWindow = createUploadWindow();
                uploadWindow.style.display = '';
            });
        }else if(document.location.pathname.startsWith('/a/')){
            registerMenuCommand('Download', downloadFunc);
        }
        break;
    case 'imgbb.com':
        if(window.location.pathname == '/'){
            registerMenuCommand('Upload', function(){
                if(!uploadWindow) uploadWindow = createUploadWindow();
                uploadWindow.style.display = '';
            });
        }
        registerMenuCommand('Download', downloadFunc);
        break;
    case 'postimages.org':
        if(window.location.pathname == '/'){
            registerMenuCommand('Upload', function(){
                if(!uploadWindow) uploadWindow = createUploadWindow();
                uploadWindow.style.display = '';
            });
        }
        break;
    case 'postimg.cc':
    case 'pixxxels.cc':
        registerMenuCommand('Download', downloadFunc);
        break;
}
